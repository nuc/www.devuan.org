module DevuanHelpers

  PROJECT_URL = "https://git.devuan.org/devuan-editors/devuan-www"
  GIT_BRANCH  = "beta.devuan.org"
  EDIT_URL    = "#{PROJECT_URL}/edit/#{GIT_BRANCH}/source"
  SRC_URL     = "#{PROJECT_URL}/tree/#{GIT_BRANCH}/source"

  def relative_source_path
    current_page.source_file.split('/source')[1]
  end

  # Override link_to to check for external links
  # and add target: _blank (#98) + rel="nofollow"
  def link_to(*args, &block)
    options  = args.extract_options!
    name     = block_given? ? '' : args.shift
    href     = args.first
    if href =~ %r[^https?://] # skip local links
      host   = href.split('/')[2]
      unless host =~ /devuan\.org$/
        options.reverse_merge!(:target => '_blank', :rel => 'nofollow')
      end
    end
    super(*[name, href, options.empty? ? nil : options].compact, &block)
  end

  def link_to_edit_page
    link_to(t(:edit), File.join(EDIT_URL, relative_source_path))
  end
  def link_to_source
    link_to(t(:view_source), File.join(SRC_URL, relative_source_path))
  end
  def link_to_new_issue
    link_to(t(:issues), "#{PROJECT_URL}/issues/new?issue[label_ids][]=1291&issue[title]=#{current_page.url}")
  end

  def mirror_countries(mirrors)
    countries = []
    (mirrors || []).sort_by { |m| m.location }.each do |m|
      country = m.location.match(/,/) ? m.location.split(',').last.strip : m.location
      countries << country unless countries.include?(country)
    end
    countries.sort
  end

  def fdo_mirror_countries
    mirror_countries data.mirrors.fdo
  end

  def wdo_mirror_countries
    mirror_countries data.mirrors.wdo
  end

  def national_mirrors(mirrors, country, partial = :mirror_fdo)
    national_mirrors = ""
    mirrors.each do |m|
      next unless m.location =~ /#{country}/
      national_mirrors << partial(partial, :locals => { m: m })
    end
    national_mirrors
  end

  def bitcoin_address
    "1QFbx3bKA8LABAGEaSe7EiP9JCxe2j4fN7".freeze
  end

  def breadcrumbs
    crumbs = current_page.path.split('/')
    last   = crumbs.pop.sub!(/(index)?\.html$/, '').to_s # Remove HTML file
    path   = ''
    crumbs.map! do |c|
      path << "/#{c}"
      link_to(c, path)
    end
    path = '<nav id="breadcrumbs">/' << crumbs.join('/')
    path << "/<strong>#{last}</strong>" unless last.empty?
    path << ' <span>' << current_page.data.title.to_s << '</span>'
    path << '</nav>'
  end

  def current_title
    current_title = "#{current_page.data.title}"
    current_title << ' | ' unless current_title.empty?
    current_title << site_title
  end

  def favicon_url
    "/ui/img/favicon.ico".freeze
  end

  def gitlab_new_issue(project, query)
    URI("https://git.devuan.org/#{project}/issues/new?#{query}").to_s
  end

  def icon(name)
    '<span class="icon icon-' << name << '"></span>'
  end

  def link_to_maintainer(package)
    mm = package.maintainer.split(',').map(&:strip)
    mm.map! do |maint|
      if maint =~ /^(.*) <(.*)>$/
        name, email = $1, $2
	"#{name} #{email}"
      else
        maint
      end
    end.join(', ')
  end

  def pkg_id(package, extra = nil)
    ['pkg', package, extra].compact.join('-')
  end

  def pkg_license(package)
    data.legal.licenses.find(package.license) do |l|
      link_to l.code, l.url
    end || "unknown license: #{l}"
  end

  def pkg_classification(pkg)
    c = [pkg.origin, pkg.section, pkg.priority, (t(:essential) if pkg.essential == 'yes')]
    content_tag('p', c.compact.join(' : '), class: 'classification')
  end

  def site_title
    I18n.t(:site_title)
  end

  def tagline
    I18n.t(:tagline)
  end

  def r(source) # render Markdown
    Tilt['markdown'].new { source }.render
  end

  def toc
    file = Pathname(File.expand_path("source/pages/" << current_page.path.chomp('.html'))).to_s
    file = file << ".en.html.md"
    unless File.exists?(file)
      file = file << ".erb"
    end
    return "" unless File.exists?(file)

    lines = File.readlines(file).select { |l| l =~ /^## / }
    lines.map! do |l|
      l.strip
       .sub(/^## /, '')
       .gsub!(/^(.*)$/) { |x| "- [#{x}](##{x.downcase.gsub(/ /, '-')})" }
    end
    lines.unshift('### Sections [ ^ ](#top)')
    lst = r(lines.join("\n"))
    toc = content_tag('nav', lst, class: 'toc')
  end

  def video_tag(url, title = nil)
    html = '<video src="%s" controls></video>' % url
    html << '<caption>%s</caption>' % title unless title.nil?
    '<figure>' << html << '</figure>'
  end
end
